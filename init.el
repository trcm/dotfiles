(require 'package) ;; You might already have this line
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line

;; (setq package-list '(counsel doom-themes all-the-icons dumb-jump evil-leader evil-nerd-commenter evil-surround exec-path-from-shell intero company ivy-rich kaolin-themes autothemer key-chord linum-relative lsp-haskell haskell-mode lsp-ui lsp-mode flycheck dash-functional magit git-commit ghub graphql magit-popup markdown-mode memoize popup ranger smart-mode-line rich-minority smartparens swiper ivy treemacs-evil evil goto-chg treemacs-projectile treemacs ht hydra pfuture ace-window avy f s dash projectile pkg-info epl treepy undo-tree use-package bind-key with-editor async yaml-mode))
;; (dolist (package package-list)
;;   (unless (package-installed-p package)
;;     (package-install package)))

;; SETUP
(setq exec-path (append exec-path '("/home/tom/dart-sdk/bin" "/home/tom/.local/bin" "/home/tom/.pub-cache/bin" "/opt/cabal/bin")))
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

(set-frame-font "Iosevka-10")
;; (load-theme 'base16-outrun-dark t)
(load-theme 'deeper-blue t)

(setq inhibit-startup-message t) ; get rid of the welcome screen
(setq system-uses-terminfo nil)
(setq redisplay-dont-pause t)
(global-hl-line-mode t)

(tool-bar-mode -1) ;; removes toolbar
(menu-bar-mode -1) ;; remove menu bar
(display-time-mode t)
(column-number-mode t) ;; display column numbers
(scroll-bar-mode -1) ;; remove scroll bars
(setq ring-bell-function 'ignore)

(add-hook 'after-init-hook 'global-company-mode)
(require 'smartparens-config)
(smartparens-global-mode t)
(show-smartparens-global-mode t)
(setq sp-autoescape-string-quote 0)
(setq sp-autoskip-closing-pair 'always)
(setq sp-highlight-pair-overlay nil)
(add-to-list 'sp-sexp-suffix (list 'js2-mode 'regexp ""))
(add-to-list 'sp-sexp-suffix (list 'rust-mode 'regexp ""))

(setq sml/no-confirm-load-theme t)
(setq sml/theme 'dark)
(sml/setup)

(require 'ivy-rich)
(ivy-mode t)
(ivy-rich-mode 1)
;; (ivy-set-display-transformer 'ivy-switch-buffer 'ivy-rich-switch-buffer-transformer)
(setq projectile-completion-system 'ivy)
(setq projectile-enable-caching t)
(projectile-global-mode t)
(global-auto-revert-mode t)

;; Backups
(setq backup-directory-alist '(("." . "/home/tom/.emacs.d/backups/")))
(setq
   backup-by-copying t      ; don't clobber symlinks
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)

;; KEYBINDINGS
;; (global-set-key (kbd "C-x 4") 'thirds)
(global-set-key "\C-x\C-m"	'counsel-M-x)
(global-set-key (kbd "C-x m")	'counsel-M-x)
(global-set-key "\C-c\C-m"	'counsel-M-x)
(global-set-key (kbd "C-c b")   'back-to-other-buffer)
;; (global-set-key (kbd "C-c i") 'counsel-semantice
;; (global-set-key (kbd "C-c d") 'rjsx-delete-creates-full-tag)
(global-set-key (kbd "C-c C-d") 'reload-and-switch-haskell)
;; (global-set-key (kbd "C-=") 'er/expand-region)

;; SMART PARENS KEYBINDING
;; (define-key smartparens-mode-map (kbd "C-'") 'sp-forward-slurp-sexp)
;; (define-key smartparens-mode-map (kbd "C-;") 'sp-forward-barf-sexp)
;; (define-key smartparens-mode-map (kbd "C-M-<left>") 'sp-backward-slurp-sexp)
;; (define-key smartparens-mode-map (kbd "C-M-<right>") 'sp-backward-barf-sexp)

;; Dumb Jump
(dumb-jump-mode)
(setq dumb-jump-selector 'ivy)

(setq ispell-program-name (executable-find "hunspell")
      ispell-dictionary "en_AU")

;; ;; COMPANY
(use-package company
  :init
  (setq company-idle-delay 0.5)
  (setq company-tooltip-limit 20)
  (setq company-minimum-prefix-length 3)
  (setq company-echo-delay 0)
  (setq company-auto-complete nil)
  (global-company-mode 1)
  (add-to-list 'company-backends 'company-dabbrev t)
  (add-to-list 'company-backends 'company-ispell t)
  (add-to-list 'company-backends 'company-files t)
  (setq company-backends (remove 'company-ropemacs company-backends)))

(require 'color)

(let ((bg (face-attribute 'default :background)))
  (custom-set-faces
   `(company-tooltip ((t (:inherit default :background ,(color-lighten-name bg 2)))))
   `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 10)))))
   `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
   `(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
   `(company-tooltip-common ((t (:inherit font-lock-constant-face))))))

;; EVIL ;;
;; (require 'linum-relative)
(use-package evil
  :init
  (require 'evil)
  (evil-mode 1)
  (global-evil-leader-mode 1)
  (global-evil-surround-mode)
  ;; (global-evil-mc-mode)
  (evilnc-default-hotkeys)
  (setq evil-leader/leader ",")
  (evil-set-initial-state 'elm-interactive-mode 'emacs)
  (evil-set-initial-state 'indium-repl-mode 'emacs)
  (evil-set-initial-state 'intero-repl-mode 'emacs)
  (evil-set-initial-state 'term-mode 'emacs)
  (evil-set-initial-state 'eshell 'emacs)
  (setq evil-insert-state-cursor '(hbar))

;;; evil leader custom keybinds
  (evil-leader/set-key
    "w"    'save-buffer
    "f"    'lsp-format-buffer
    "a"    'avy-goto-char
    "s"    'avy-goto-word-1
    "z"    'avy-goto-line
    "b"    'ido-switch-buffer
    "i"    'indent-region
    "k"    'kill-buffer
    "r"    'revert-buffer
    "m"    'mark-whole-buffer
    "d"    'counsel-grep-or-swiper
    "o"    'back-to-other-buffer
    "e"    'eval-buffer
    "g"    'projectile-switch-project
    "j"    'counsel-git
    "b"    'switch-to-buffer
    "n"    'treemacs
    "p"    'counsel-git-grep
    "t"    'ranger
    "ci"   'evilnc-comment-or-uncomment-lines
    "q"    'ace-window
    "xf"   'xref-find-definitions
    "h"    'haskell-hoogle
    )

  (require 'key-chord)
  (key-chord-mode 1)
  (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
  (key-chord-define evil-visual-state-map "jk" 'evil-normal-state)
  )
(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs              (if (executable-find "python") 3 0)
          treemacs-deferred-git-apply-delay   0.5
          treemacs-display-in-side-window     t
          treemacs-file-event-delay           5000
          treemacs-file-follow-delay          0.2
          treemacs-follow-after-init          t
          treemacs-follow-recenter-distance   0.1
          treemacs-goto-tag-strategy          'refetch-index
          treemacs-indentation                2
          treemacs-indentation-string         " "
          treemacs-is-never-other-window      nil
          treemacs-no-png-images              t
          treemacs-project-follow-cleanup     nil
          treemacs-persist-file               (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-recenter-after-file-follow nil
          treemacs-recenter-after-tag-follow  nil
          treemacs-show-hidden-files          t
          treemacs-silent-filewatch           nil
          treemacs-silent-refresh             nil
          treemacs-sorting                    'alphabetic-desc
          treemacs-space-between-root-nodes   t
          treemacs-tag-follow-cleanup         t
          treemacs-tag-follow-delay           1.5
          treemacs-width                      35)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null (executable-find "python3"))))
      (`(t . t)
       (treemacs-git-mode 'extended))
      (`(t . _)
       (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-evil
  :after treemacs evil
  :ensure t)

(use-package treemacs-projectile
  :after treemacs projectile
  :ensure t)

;; Haskell
(use-package haskell-mode
  :defer t
  :init
  (require 'lsp-ui)
  (require 'lsp-haskell)
  ;; (add-hook 'lsp-mode-hook 'lsp-ui-mode)
  (add-hook 'haskell-mode-hook #'lsp)
  ;; (add-hook 'haskell-mode-hook 'dante-mode)
  ;; (add-hook 'haskell-mode-hook 'intero-mode)
  ;; (add-hook 'haskell-mode-hook 'flycheck-mode)
  (with-eval-after-load 'intero
    (flycheck-add-next-checker 'intero '(warning . haskell-hlint))
    )
  (setq haskell-process-args-ghci
	'("-ferror-spans" "-fshow-loaded-modules"))

  (setq haskell-process-args-cabal-repl
	'("--ghc-options=-ferror-spans -fshow-loaded-modules"))

  (setq haskell-process-args-stack-ghci
	'("--ghci-options=-ferror-spans -fshow-loaded-modules"
	  "--no-build" "--no-load"))

  (setq haskell-process-args-cabal-new-repl
	'("--ghc-options=-ferror-spans -fshow-loaded-modules"))
  )
(defun format-haskell-on-save ()
  "Function formats haskell buffer with brittany on save."
  (when (eq major-mode 'haskell-mode)
    (shell-command-to-string (format "brittany --write-mode inplace %s" buffer-file-name))
    (revert-buffer :ignore-auto :noconfirm)))
;; (add-hook 'after-save-hook #'format-haskell-on-save)
(add-hook 'purescript-mode-hook 'turn-on-purescript-indentation)
(add-hook 'purescript-mode-hook 'psc-ide-mode)

(setq temporary-file-directory "~/.emacs.d/tmp/")

;; Rust

(require 'lsp-rust)

;; FUNCTIONS

(defun back-to-other-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer)))

(defun reload-and-switch-haskell ()
  (interactive)
  (haskell-process-load-or-reload)
  (haskell-interactive-switch))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#3c3836" "#fb4933" "#b8bb26" "#fabd2f" "#83a598" "#d3869b" "#8ec07c" "#ebdbb2"])
 '(beacon-color "#dc322f")
 '(company-backends
   (quote
    (dante-company company-bbdb company-eclim company-semantic company-clang company-xcode company-cmake company-capf company-files
		   (company-dabbrev-code company-gtags company-etags company-keywords)
		   company-oddmuse company-dabbrev company-ispell)))
 '(company-quickhelp-color-background "#3E4452")
 '(company-quickhelp-color-foreground "#ABB2BF")
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-safe-themes
   (quote
    ("f92f181467b003a06c3aa12047428682ba5abe4b45e0fca9518496b9403cde6f" "6daa09c8c2c68de3ff1b83694115231faa7e650fdbb668bc76275f0f2ce2a437" "3a3de615f80a0e8706208f0a71bbcc7cc3816988f971b6d237223b6731f91605" "f6f5d5adce1f9a764855c9730e4c3ef3f90357313c1cae29e7c191ba1026bc15" "e2fd81495089dc09d14a88f29dfdff7645f213e2c03650ac2dd275de52a513de" "b73a23e836b3122637563ad37ae8c7533121c2ac2c8f7c87b381dd7322714cd0" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "7e78a1030293619094ea6ae80a7579a562068087080e01c2b8b503b27900165c" "4cf3221feff536e2b3385209e9b9dc4c2e0818a69a1cdb4b522756bcdf4e00a4" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" "5dc98c649900306c934d94c0113e54702f3904bf3784f553a33381d74b3b7e1d" "edea0b35681cb05d1cffe47f7eae912aa8a930fa330f8c4aeb032118a5d0aabf" "f71859eae71f7f795e734e6e7d178728525008a28c325913f564a42f74042c31" "58f6848b780dd5c36c17b45e42920eac19a55338d2ae716cef830b5193545611" "d890583c83cb36550c2afb38b891e41992da3b55fecd92e0bb458fb047d65fb3" "93a0885d5f46d2aeac12bf6be1754faa7d5e28b27926b8aa812840fe7d0b7983" "6b2636879127bf6124ce541b1b2824800afc49c6ccd65439d6eb987dbf200c36" default)))
 '(dante-project-root nil)
 '(dante-repl-command-line (quote (cabal new-repl --builddir=dist/dante)))
 '(fci-rule-color "#5B6268")
 '(flycheck-hlintrc "~/.config/hlint.yaml")
 '(frame-background-mode (quote dark))
 '(global-evil-mc-mode t)
 '(haskell-mode-stylish-haskell-path "stylish-haskell")
 '(haskell-stylish-on-save t)
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#002b36" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#546E00" . 20)
     ("#00736F" . 30)
     ("#00629D" . 50)
     ("#7B6000" . 60)
     ("#8B2C02" . 70)
     ("#93115C" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(hl-paren-colors (quote ("#2aa198" "#b58900" "#268bd2" "#6c71c4" "#859900")))
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(linum-relative-current-symbol "")
 '(linum-relative-global-mode t)
 '(lsp-prefer-flymake nil)
 '(magit-diff-use-overlays nil)
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(package-selected-packages
   (quote
    (spaceline dhall-mode smooth-scrolling flycheck-pos-tip nix-sandbox nix-mode rainbow-mode auto-highlight-symbol evil-mc flycheck-purescript purescript-mode one-themes spacemacs-theme sublimity color-theme-sanityinc-solarized solarized-theme color-theme-solarized racket-mode zeno-theme challenger-deep-theme lsp-mode haskell-mode company ac-ispell yaml-mode use-package treemacs-projectile treemacs-evil solaire-mode smartparens smart-mode-line smart-jump shakespeare-mode restclient ranger psci psc-ide paganini-theme origami omnibox nimbus-theme magit lsp-ui lsp-rust lsp-haskell linum-relative key-chord kaolin-themes json-mode js2-mode ivy-rich intero inkpot-theme hindent gruvbox-theme gotham-theme flycheck-haskell exec-path-from-shell evil-surround evil-nerd-commenter evil-leader eglot doom-themes deft dart-mode dante danneskjold-theme cyberpunk-theme counsel company-lsp base16-theme alchemist)))
 '(pdf-view-midnight-colors (quote ("#fdf4c1" . "#1d2021")))
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(safe-local-variable-values
   (quote
    ((dante-project-root "~/Programming/haskell/hs-chat")
     (dante-project-root . "~/Programming/haskell/hs-chat"))))
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(sublimity-mode t)
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(vc-annotate-background "#282c34")
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (list
    (cons 20 "#98be65")
    (cons 40 "#b4be6c")
    (cons 60 "#d0be73")
    (cons 80 "#ECBE7B")
    (cons 100 "#e6ab6a")
    (cons 120 "#e09859")
    (cons 140 "#da8548")
    (cons 160 "#d38079")
    (cons 180 "#cc7cab")
    (cons 200 "#c678dd")
    (cons 220 "#d974b7")
    (cons 240 "#ec7091")
    (cons 260 "#ff6c6b")
    (cons 280 "#cf6162")
    (cons 300 "#9f585a")
    (cons 320 "#6f4e52")
    (cons 340 "#5B6268")
    (cons 360 "#5B6268")))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83")))
 '(xterm-color-names
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"])
 '(xterm-color-names-bright
   ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-scrollbar-bg ((t (:background "#2bd12f784561"))))
 '(company-scrollbar-fg ((t (:background "#21e824bc35b0"))))
 '(company-tooltip ((t (:inherit default :background "#1bf61e4b2c46"))))
 '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
 '(company-tooltip-selection ((t (:inherit font-lock-function-name-face)))))


